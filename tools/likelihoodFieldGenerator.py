import numpy as np
import os
#import octomap
import open3d as o3d
import easygui

# Get path to current file directory
cwd = os.path.dirname(os.path.abspath(__file__))


def getMeshFromModel_GUI():
    '''
        Input:

        Output:
            mesh    -   instance of mesh read from model

        Opens a GUI instance, takes in a mesh in the form of .stl, .ply or other supported mesh files 
        (http://www.open3d.org/docs/release/tutorial/geometry/file_io.html)
    '''

    # Use easygui to get file
    meshFile = easygui.fileopenbox(msg=None, title=None, default=cwd + "/../models/")

    # Import ply model as triangular mesh
    mesh = o3d.io.read_triangle_mesh(meshFile) 

    return mesh

def createPCDFromModel_GUI(nPoints):
    '''
        Input:
            nPoints -   Number of points to uniformally sample the mesh with

        Output:
            pcloud  -   Pint cloud with nPoints sampled uniformly over chosen model

        Opens a GUI instance, takes in a mesh in the form of .stl, .ply or other supported mesh files 
        (http://www.open3d.org/docs/release/tutorial/geometry/file_io.html)
    '''
    # Open gui instance to get mesh
    mesh = getMeshFromModel_GUI()

    # Sample point off the mesh uniformally
    pcloud = o3d.geometry.TriangleMesh.sample_points_uniformly(mesh, nPoints) 

    return pcloud

def createKDTreeFromMesh_GUI(nPoints):
    '''
        Input:
            nPoints -   Number of points to uniformally sample the mesh with

        Output:
            kdtree  -   Instance of kd-tree

        Opens a GUI instance, takes in a mesh in the form of .stl, .ply or other supported mesh files 
        (http://www.open3d.org/docs/release/tutorial/geometry/file_io.html)
    '''

    # Get Point cloud
    pcloud = createPCDFromModel_GUI(nPoints)

    # Create KD-Tree
    kdtree = o3d.geometry.KDTreeFlann(pcloud)

    return kdtree

def createKDTreeFromMesh(mesh, nPoints):
    '''
        Input:
            mesh    -   Mesh to create KD-tree from
            nPoints -   Number of points to uniformally sample the mesh with

        Output:
            kdtree  -   Instance of kd-tree

        Opens a GUI instance, takes in a mesh in the form of .stl, .ply or other supported mesh files 
        (http://www.open3d.org/docs/release/tutorial/geometry/file_io.html)
    '''

    # Sample points off the mesh uniformally
    pcloud = o3d.geometry.TriangleMesh.sample_points_uniformly(mesh, nPoints)

    # Create KD-Tree
    kdtree = o3d.geometry.KDTreeFlann(pcloud)

    return kdtree

def createOctomapFromMesh_GUI(res, nPoints):
    
    '''
        Input: 
            res     -   desired map resolution
            nPoints -   Number of points to uniformally sample the mesh with

        Returns
            omap    -   Instance of octomap

        Opens a GUI instance, takes in a mesh in the form of .stl, .ply or other supported mesh files 
        (http://www.open3d.org/docs/release/tutorial/geometry/file_io.html)     
    '''

    # Setup the tree with desired resolution
    omap = octomap.OcTree(res)

    # Sample points off the mesh uniformally
    pcloud = createPCDFromModel_GUI(nPoints)

    # Convert pointcloud to array
    pcloud_np = np.asarray(pcloud.points)

    # Insert point cloud into the octree (array is "sensor origin")
    omap.insertPointCloud(pcloud_np ,np.array([0.0, 0.0, 0.0])) 

    return omap

def createOctomapFromMesh(mesh, res, nPoints):
    '''
        Input: 
            res     -   desired map resolution
            nPoints -   Number of points to uniformally sample the mesh with

        Returns
            omap    -   Instance of octomap

        Opens a GUI instance, takes in a mesh in the form of .stl, .ply or other supported mesh files 
        (http://www.open3d.org/docs/release/tutorial/geometry/file_io.html)     
    '''

    # Setup the tree with desired resolution
    omap = octomap.OcTree(res)

    # Sample points off the mesh uniformally
    pcloud = o3d.geometry.TriangleMesh.sample_points_uniformly(mesh, nPoints)

    # Convert pointcloud to array
    pcloud_np = np.asarray(pcloud.points)

    # Insert point cloud into the octree (array is "sensor origin")
    omap.insertPointCloud(pcloud_np ,np.array([0.0, 0.0, 0.0])) 

    return omap

def createGridMap(mesh, resolution, nPoints, sig_sens):
    '''
        Input:
            mesh        -   mesh to base map on (Open3D triangle mesh)
            resulotion  -   Map resolution (float)
            nPoints     -   number of points to sample the map with

        Returns:
            mapArray    -   3D array containing distances to closest points in model
            minBound    -   array with the offset from model origin to cell [0, 0, 0]


        Function to create a gridmap (3D-Array) where each voxel contains the distance to the closest point in the map 
        (Prelude to Likelihood-map)

        TODO: Automatically calculate nPoints from resolution and size of mesh.
    '''
    # Get boundary box of model
    maxBound = np.array(mesh.get_max_bound(), dtype=np.float32) + 6*sig_sens
    minBound = np.array(mesh.get_min_bound(), dtype=np.float32) - 6*sig_sens
    [x_size, y_size, z_size] = np.round_(maxBound - minBound, 3)
    boundaryBox = np.array([x_size, y_size, z_size])
    print("Maxbound: " + str(maxBound))
    print("Minbound: " + str(minBound))
    print(boundaryBox)

    # Find size of array to cover the boundary-box with voxels at given resolution
    [x_idxSize, y_idxSize, z_idxSize] = np.int32(np.ceil(boundaryBox/resolution))
    gridSize = np.array([x_idxSize, y_idxSize, z_idxSize], dtype=np.int32)
    print("Size of map in [cells]" + str(gridSize))

    # Get KD-tree from mesh
    kdTree = createKDTreeFromMesh(mesh, nPoints)

    # Setup 3D array for map with given resolution
    mapArray = np.ones(gridSize)

    # Create vectors of all voxel center coordinates
    '''
        Linspace(l, u, n) splits [l, u] into n equally sized pieces, add resolution/2 to get voxel center coordinate
        np.round_(num, dec) rounds num to dec decimal places
    '''
    linspaceXVoxelCenter = np.round_(np.linspace(minBound[0], maxBound[0], x_idxSize, endpoint=False) + resolution/2, 3)
    linspaceYVoxelCenter = np.round_(np.linspace(minBound[1], maxBound[1], y_idxSize, endpoint=False) + resolution/2, 3)
    linspaceZVoxelCenter = np.round_(np.linspace(minBound[2], maxBound[2], z_idxSize, endpoint=False) + resolution/2, 3)

    print("Beginning loop, this might take a while...")

    # Get distances from center of each voxel to nearest point in space
    for idx_z, z in enumerate(linspaceZVoxelCenter):
        print("Status: " + str(np.round_((idx_z / z_idxSize * 100),2)) + " Percent finished")
        for idx_y, y in enumerate(linspaceYVoxelCenter):
            for idx_x, x in enumerate(linspaceXVoxelCenter):            
                # Search KD-tree for closest neighbour in all voxels
                
                [k, idx, sqdist] = kdTree.search_hybrid_vector_3d([[x], [y], [z]], 6*sig_sens, 1)
                
                # If nothing is found withing 6*sig_sens, sqdist will be an empty vector
                # Appending a value of ((6*sig_sens)**2) to the end of the list will fix the problem arising
                # when no points are found within max search range, 6 sigma is chosen because then the probability
                # of hit will be essentially 0
                sqdist.append((6*sig_sens)**2)  

                # Appending is done because then no check has to be made, if something is found within 6*sigma,
                # The appended value will have index [1], and not be used, if nothing is found, it will
                # have index [0], and be used.

                # Find distance to closest point by taking the square root of sqdist[0]
                dist = np.sqrt(sqdist[0])
                mapArray[idx_x][idx_y][idx_z] = dist
                #print("X: " + str(x) + "    Y: " + str(y) + "   Z: " + str(z) + "   Dist: " + str(dist))
    
    print("Loop finished!")

    #print(gridSize)

    mapMetaData = np.array([["Coordinate of cell [0, 0, 0] relative to origin [in m]", minBound],
                            ["Resolution of map [in m]", resolution],
                            ["Size of Gridmap [in cells]", gridSize]],dtype=object)

    return mapArray, mapMetaData

def generateLikelihoodMap(distmap, sigma_sens, uint8Map = False):
    '''
        Input:
            distmap         -   Map with distances to closest point in mesh (3D np.array)
            sigma_sens      -   Standard deviation of sensor used (float)
            uint8Map        -   Bool to say if probabilities are to be saved as uint8's (0-255, to be parsed)

        Output:
            likelihoodMap   -   Gridmap with likelihoods [3D np.array]
            maxVal          -   Max value of gaussian

        Function to generate a likelihood-map from a distance-map
    '''
    # Find maximal possible value of gaussian 
    maxVal = 1.0/(sigma_sens*np.sqrt(2.0*np.pi)) 

    # Create likelihood map
    likelihoodMap = maxVal * np.exp(-1.0/2.0*(distmap/sigma_sens)**2)

    if uint8Map:
        # If the data type specified is uint8, scale map to the interval [0, 255]
        likelihoodMapScaled = likelihoodMap * 255/maxVal

        # Round off to 0 decimals
        likelihoodMapScaled = np.round_(likelihoodMapScaled, decimals=0)

        # Cast to uint8
        likelihoodMap = likelihoodMapScaled.astype(np.uint8)


    return likelihoodMap, maxVal

def saveOctoMap(tree, name):
    '''
        Input:
            tree    -   Octomap to save
            name    -   Filename to save the map as
    '''
    if tree.write(name.encode('utf-8')): # Write binary OctoMap file (Only encode "occupied", "free" and "unknown")
        print("Octomap Created from file at chosen path")
    else:
        print("Cannot create octree file.")

if __name__ == "__main__":

    # Get mesh from model
    mesh = getMeshFromModel_GUI()

    # Setting resolution and number of points to sample mesh with
    resolution = np.float32(easygui.enterbox(msg="Resolution of map (voxel side-length), [m]", default=0.1))
    nPoints = 10000000
    sig_sens = np.float32(easygui.enterbox(msg="Std deviation of map gaussian, [m]", default=0.1))
    mapDataType = np.float32(easygui.enterbox(msg="Datatype to use (float32 / uint8): [0: float32, 1: uint8]", default=0))
    mapDataType = (mapDataType > 0.5) # As easygui only returns strings, a check is done to get the desired bool

    # Name files
    mapName = str(easygui.enterbox(msg="Desired map name", default="map"))
    metaDataName = str(easygui.enterbox(msg="Desired metadata name", default="metadata"))

    # Create grid map
    arrayMap, mapMetaData = createGridMap(mesh, resolution, nPoints, sig_sens)

    # Convert to likelihood map
    likmap, maxVal = generateLikelihoodMap(arrayMap, sig_sens, uint8Map=mapDataType)

    # Append items to mapMetaData
    maxValEntry = np.array(["Maximum value of map gaussian", maxVal], dtype=object)
    dataTypeEntry = np.array(["Uint8 used in map (True / False :: uint8 / float32)", mapDataType], dtype=object)
    appendArr = np.array([  [maxValEntry],
                            [dataTypeEntry]], dtype=object).reshape((2,2))

    mapMetaData = np.append(mapMetaData, appendArr, axis=0)

    print(mapMetaData)

    #octomap = createOctomapFromMesh(mesh, resolution, nPoints)
    #saveOctoMap(octomap, "likelihoodMap.ot")
    #arrayMap = np.load("test.npy")

    # Save 3D numpy array
    np.save(cwd +  "/../maps/" + mapName ,likmap)
    np.save(cwd +  "/../maps/" + metaDataName, mapMetaData)


    