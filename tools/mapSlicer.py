import numpy as np
import easygui
import png
import os

# Path to current folder
cwd = os.path.dirname(os.path.abspath(__file__))

# Load map
mapFile = easygui.fileopenbox(msg="Choose map", title=None, default=cwd + "/../maps/")
likMap = np.load(mapFile)

# Load metadata
metaDataFile = easygui.fileopenbox(msg="Choose metaData", title=None, default=cwd + "/../maps/")
metaData = np.load(metaDataFile, allow_pickle=True)

# Parse needed stuff from metadata
mapOffset = metaData[0,1]
mapRes = metaData[1,1]

# Set height of desired slice
sliceHeight = np.float32(easygui.enterbox(msg="Height at which to slice map, [m]"))

# Get slice
sliceIdx = np.int32(np.round_((sliceHeight-mapOffset[2])/mapRes, decimals = 0))
mapSlice = np.sqrt(likMap[:,:,sliceIdx])
maxVal = np.amax(mapSlice)
mapSlice = mapSlice/maxVal * 255

# Convert to uint8
mapSliceImgArray = mapSlice.astype(np.uint8)

imageName = str(easygui.enterbox(msg="Name of .png image [do not add .png at the end]"))

# Save image
png.from_array(mapSliceImgArray, mode="L").save(cwd +  "/../mapSlices/" + imageName + ".png")