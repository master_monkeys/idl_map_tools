Repo containing the map creation script and associated tools for visualizing a map slice

Models (.stl, .ply etc) should be stored in the "models" folder

The map generation script outputs the map and metadata files to the "maps" folder and .npy files (numpy arrays)

The map slicer file slices the chosen likelihood map at the desired height and outputs the slice as a .png file in the "mapSlices" folder


**Required python packages:**

**Map Generation:**
- Open3D
- easygui
- numpy

**Map slice visualization:**
- pyPNG
- easygui
- numpy

**Optional:**
- Octomap
